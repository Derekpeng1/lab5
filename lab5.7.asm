.data
word1: .word 0x89abcdef
#word1 in memory :
# 10010000  ef
# 10010001  cd
# 10010002  ab
# 10010003  89

.text
.globl main
main:
	la $a0, word1
	lwl $t0, 0($a0)
	lwl $t1, 1($a0)
	lwl $t2, 2($a0)
	lwl $t3, 3($a0)
	jr $ra
