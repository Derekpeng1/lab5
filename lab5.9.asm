.data 0x10000000
.align 0
char1:	.byte 'a'	# reserve space for a byte
double1:	.double 1.1	# reserve space for a double
char2:	.byte 'b'	# b is 0x62 in ASCII
half1:	.half 0x8001	# reserve space for a half-word (2 bytes)
char3:	.byte 'c'	# c is 0x63 in ASCII
word1:	.word 0x56789abc	# reserve space for a word
char4:	.byte 'd'	# d is 0x64 in ASCII

.text	
.globl main	
main:	
	addu $s0, $ra, $0
	lui $a1, 0x1000
	ori $a0, $a1, 0xd
	lwl $t0, 3($a0)
	addu $0, $0, $0
	lwr $t0, 0($a0)
	addu $0, $0, $0
	addu $ra, $s0, $0
	jr $ra
	addu $0, $0, $0
	