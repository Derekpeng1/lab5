.data 0x10000000
.align 0
ch1: .byte 'a'	# reserve space for a byte
word1: .word 0x89abcdef
ch2: .byte 'b'
word2: .word 0

.text	
.globl main	
main:	
	addu $s0, $ra, $0
	lui $t1, 0x1000
	ori $a0, $t1, 1
	lui $t1, 0x1000
	ori $a1, $t1, 6
	lwl $t0, 3($a0)
	addu $0, $0, $0	
	lwr $t0, 0($a0)
	addu $0, $0, $0
	swl $t0, 3($a1)
	addu $0, $0, $0
	swr $t0, 0($a1)
	addu $0, $0, $0
	addu $ra, $s0, $0
	jr $ra
	addu $0, $0, $0
	