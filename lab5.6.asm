.data
user1: .word 0
msg: .asciiz "If bytes were layed in reverse order the number would be: "


.text
.globl main
main:
  addi $sp, $sp, -4  # substract 4 from stack pointer
  sw $ra, 4($sp)
  li $v0, 5
  syscall
  sw $v0, user1($0)
  la $a0, user1
  jal Reverse_bytes
  li $v0, 4
  la $a0, msg   
  syscall
  li $v0, 1
  lw $a0, user1($0)
  syscall	
  lw $ra, 4($sp)
  addi $sp ,$sp, 4
  jr $ra

Reverse_bytes:
  lb $t0, 0($a0)  #swap lowest and highest byte
  lb $t1, 3($a0)
  sb $t0, 3($a0)
  sb $t1, 0($a0)
  lb $t0, 1($a0)  #swap middle bytes
  lb $t1, 2($a0)
  sb $t0, 0($a0)
  sb $t1, 1($a0)
  jr $ra